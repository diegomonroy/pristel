// JavaScript Document

/* ************************************************************************************************************************

Pristel

File:			app.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

/* Foundation */

$(document).foundation();

/* WOW */

new WOW().init();

/* jQuery */

jQuery.noConflict();

/* Add ID */

function addID( element, id ) {
	element = jQuery( element );
	element.attr( 'id', id );
}

/* Animation on Click */

function animationClick( element, section, animation, speed, space ) {
	element = jQuery( element );
	element.click(
		function () {
			jQuery( section ).animatescroll({
				easing: animation,
				scrollSpeed: speed,
				padding: space
			});
		}
	);
}

jQuery(document).ready(function () {

	/* AnimateScroll */

	addID( 'li.item-111 a', 'link_about_us' );
	animationClick( '#link_about_us', '#about_us', 'easeOutBounce', 2000, 94 );
	addID( 'li.item-112 a', 'link_portfolio' );
	animationClick( '#link_portfolio', '#portfolio', 'easeOutBounce', 2000, 94 );
	addID( 'li.item-113 a', 'link_product' );
	animationClick( '#link_product', '#product', 'easeOutBounce', 2000, 94 );
	addID( 'li.item-121 a', 'link_support' );
	animationClick( '#link_support', '#support', 'easeOutBounce', 2000, 94 );
	addID( 'li.item-114 a', 'link_contact_us' );
	animationClick( '#link_contact_us', '#contact_us', 'easeOutBounce', 2000, 94 );

	/* ImageMapster */

	jQuery( '#Colombia' ).mapster({
		singleSelect: true,
		render_highlight: {
			altImage: 'images/pristel/contents/mapa_hover.png'
		},
		mapKey: 'name',
		fill: true,
		altImage: 'images/pristel/contents/mapa_hover.png',
		fillOpacity: 1,
		onClick: function () {
			var $button_id = jQuery( this ).attr( 'id' );
			var $button_href = jQuery( this ).attr( 'href' );
			var $style_length = jQuery( '.active_city' ).length;
			var $active_id = '#' + jQuery( '.active_city' ).attr( 'id' );
			var $active_content = jQuery( $active_id );
			var $new_id = '#content_' + $button_id;
			var $new_content = jQuery( $new_id );
			/*jQuery( 'body, html' ).animate({
				scrollTop: jQuery( $button_href ).offset().top
			}, 600);*/
			if ( $style_length > 0 ) {
				$active_content.toggle().toggleClass( 'active_city' );
			}
			$new_content.toggle().toggleClass( 'active_city' );
			if ( $active_id == $new_id ) {
				$new_content.toggle().toggleClass( 'active_city' );
			}
		}
	});

	/* Foundation */

	jQuery( '#nombre, #correo, #mensaje' ).foundation( 'destroy' );

});