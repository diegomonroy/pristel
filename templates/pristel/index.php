<?php

/* ************************************************************************************************************************

Pristel

File:			index.php
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

defined( '_JEXEC' ) or die( 'Acceso Restringido.' );

JHtml::_( 'behavior.framework', true );

// Variables

$site_base = $_SERVER['HTTP_HOST']; // e.g. www.amapolazul.com
$site_path = 'http://' . $site_base; // e.g. http://www.amapolazul.com
$app = JFactory::getApplication();
$option = JRequest::getVar( 'option' );
$view = JRequest::getVar( 'view' );
$Itemid = JRequest::getVar( 'Itemid' );
$pageclass = $app->getParams( 'com_content' );
$class = $pageclass->get( 'pageclass_sfx' );

// Template path

$path = 'templates/' . $this->template . '/';

// Modules

$show_about_us = $this->countModules( 'about_us' );
$show_banner = $this->countModules( 'banner' );
$show_bottom = $this->countModules( 'bottom' );
$show_contact_us = $this->countModules( 'contact_us' );
$show_menu = $this->countModules( 'menu' );
$show_portfolio = $this->countModules( 'portfolio' );
$show_product = $this->countModules( 'product' );
$show_support = $this->countModules( 'support' );
$show_top = $this->countModules( 'top' );
$show_welcome = $this->countModules( 'welcome' );

// Params

?>
<!DOCTYPE html>
<html class="no-js" lang="<?php echo $this->language; ?>">
	<head>
		<jdoc:include type="head" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="google-site-verification" content="-va0MHyeEobIVMS7DMRJeFp-2YBv7RlXoLXJAtd8V7U">
		<!-- Begin Open Graph Protocol -->
		<meta property="og:url" content="<?php echo $site_path; ?>">
		<meta property="og:type" content="website">
		<meta property="og:title" content="<?php echo $app->getCfg( 'sitename' ); ?>">
		<meta property="og:description" content="<?php echo $app->getCfg( 'MetaDesc' ); ?>">
		<meta property="og:image" content="<?php echo $site_path; ?>/<?php echo $path; ?>build/logo_ogp.png">
		<link rel="image_src" href="<?php echo $site_path; ?>/<?php echo $path; ?>build/logo_link_ogp.png">
		<!-- End Open Graph Protocol -->
		<link href="<?php echo $path; ?>build/bower_components/foundation-sites/dist/css/foundation-flex.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>build/bower_components/animate.css/animate.min.css" rel="stylesheet">
		<link href="<?php echo $path; ?>build/bower_components/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" media="screen">
		<link href="<?php echo $path; ?>build/app.css" rel="stylesheet" type="text/css">
		<!-- Begin Google Analytics -->
		<script>

			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-74928858-1', 'auto');
			ga('send', 'pageview');

		</script>
		<!-- End Google Analytics -->
	</head>
	<body>
		<?php if ( $show_top ) : ?>
		<!-- Begin Top -->
			<section id="top" class="top wow fadeIn" data-wow-delay="0.5s">
				<div class="row align-center">
					<jdoc:include type="modules" name="top" style="xhtml" />
				</div>
			</section>
		<!-- End Top -->
		<?php endif; ?>
		<?php if ( $show_menu ) : ?>
		<!-- Begin Menu -->
			<section id="menu_wrap" class="menu_wrap wow fadeIn show-for-small-only" data-wow-delay="0.5s">
				<div class="top-bar">
					<div class="top-bar-title">
						<span data-responsive-toggle="responsive-menu" data-hide-for="medium">
							<button class="menu-icon" type="button" data-toggle></button>
							<strong>Menú</strong>
						</span>
					</div>
					<div id="responsive-menu">
						<div class="top-bar-left">
							<jdoc:include type="modules" name="menu" style="xhtml" />
						</div>
					</div>
				</div>
			</section>
		<!-- End Menu -->
		<?php endif; ?>
		<?php if ( $show_banner ) : ?>
		<!-- Begin Banner -->
			<section id="banner" class="banner wow fadeIn" data-wow-delay="0.5s">
				<div class="row">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="banner" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Banner -->
		<?php endif; ?>
		<?php if ( $show_welcome ) : ?>
		<!-- Begin Welcome -->
			<section id="welcome" class="welcome wow fadeIn" data-wow-delay="0.5s">
				<div class="row">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="welcome" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Welcome -->
		<?php endif; ?>
		<?php if ( $show_about_us ) : ?>
		<!-- Begin About Us -->
			<section id="about_us" class="about_us wow fadeIn" data-wow-delay="0.5s">
				<div class="row">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="about_us" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End About Us -->
		<?php endif; ?>
		<?php if ( $show_portfolio ) : ?>
		<!-- Begin Portfolio -->
			<section id="portfolio" class="portfolio wow fadeIn" data-wow-delay="0.5s">
				<div class="row">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="portfolio" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Portfolio -->
		<?php endif; ?>
		<?php if ( $show_product ) : ?>
		<!-- Begin Product -->
			<section id="product" class="product wow fadeIn" data-wow-delay="0.5s">
				<div class="row">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="product" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Product -->
		<?php endif; ?>
		<?php if ( $show_support ) : ?>
		<!-- Begin Support -->
			<section id="support" class="support wow fadeIn" data-wow-delay="0.5s">
				<div class="row">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="support" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Support -->
		<?php endif; ?>
		<?php if ( $show_contact_us ) : ?>
		<!-- Begin Contact Us -->
			<section id="contact_us" class="contact_us wow fadeIn" data-wow-delay="0.5s">
				<div class="row">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="contact_us" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Contact Us -->
		<?php endif; ?>
		<?php if ( $show_bottom ) : ?>
		<!-- Begin Bottom -->
			<section id="bottom" class="bottom wow fadeIn" data-wow-delay="0.5s">
				<div class="row align-center align-middle">
					<div class="small-12 columns">
						<jdoc:include type="modules" name="bottom" style="xhtml" />
					</div>
				</div>
			</section>
		<!-- End Bottom -->
		<!-- Begin Copyright -->
			<section id="copyright_wrap" class="copyright_wrap wow fadeIn" data-wow-delay="0.5s">
				<div class="copyright">
					&copy; <?php echo date( 'Y' ); ?> <a href="<?php echo $this->baseurl; ?>"><?php echo $app->getCfg( 'sitename' ); ?></a>. Todos los derechos reservados. Sitio desarrollado por <a href="http://www.amapolazul.com" target="_blank">Amapolazul</a>.
				</div>
			</section>
		<!-- End Copyright -->
		<?php endif; ?>
		<!-- Begin Main Scripts -->
			<script src="<?php echo $path; ?>build/bower_components/jquery/dist/jquery.min.js"></script>
			<script src="<?php echo $path; ?>build/bower_components/what-input/dist/what-input.min.js"></script>
			<script src="<?php echo $path; ?>build/bower_components/foundation-sites/dist/js/foundation.min.js"></script>
			<script src="<?php echo $path; ?>build/bower_components/animatescroll/animatescroll.min.js"></script>
			<script src="<?php echo $path; ?>build/bower_components/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
			<script src="<?php echo $path; ?>build/bower_components/ImageMapster/dist/jquery.imagemapster.min.js"></script>
			<script src="<?php echo $path; ?>build/bower_components/wow/dist/wow.min.js"></script>
			<script src="<?php echo $path; ?>build/app.js" type="text/javascript"></script>
		<!-- End Main Scripts -->
	</body>
</html>